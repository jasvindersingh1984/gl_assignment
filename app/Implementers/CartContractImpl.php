<?php

namespace App\Implementers;

use App\Contracts\CartContract;
use App\Implementers\BaseImplementer;
use Illuminate\Http\Response;
use App\Http\Models\Cart;
use App\Events\ItemAdded;

class CartContractImpl extends BaseImplementer implements CartContract {

    

    /**
     * 
     * Provide detailed information of expert
     * 
     * @param $data contains expert_id to find detailed information of expert
     * 
     * @throws Exception If something happens during the process
     * 
     * @return user(expert) object with success if every thing went right otherwise failure 
     * 
     */
    public function addtolist($request) {
        try {
            $cart = new Cart;
            $cart->addToCart($request->all());
            
            event(new ItemAdded($cart));
            return $this->renderSuccess(trans('messages.item_added_successfully'), $cart);
        } catch (\Exception $e) {
            $this->logError(__METHOD__, $e);
            return $this->renderFailure(trans('messages.error.exception'), Response::HTTP_OK);
        }
    }

    /**
     * 
     * Does provide profile info of user
     * 
     * @param NULL
     * 
     * @throws Exception If something happens during the process
     * 
     * @return user object 
     * 
     */
    public function viewList() {
        try {
            $cart = Cart::all();
            $response = ['total' => $cart->sum('calories'), 'items' => $cart];
            return $this->renderSuccess(trans('messages.list_retrieved_successfully'), $response);
        } catch (\Exception $e) {
            UtilityHelper::logException(__METHOD__, $e);
            return $this->renderFailure(trans('messages.error.exception'), Response::HTTP_OK);
        }
    }

}
