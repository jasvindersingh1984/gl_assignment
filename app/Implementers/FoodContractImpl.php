<?php

namespace App\Implementers;

use App\Contracts\FoodContract;
use App\Implementers\BaseImplementer;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Config;
use App\Http\Models\Cart;
use App\Http\Resources\FoodCollection;

class FoodContractImpl extends BaseImplementer implements FoodContract {

    public $apiObj;
    public $apiConfig;
    
    public function __construct() {
        
        $this->apiConfig = Config::get('services.usda');
        $this->apiObj = new Client();
        
    }

    /**
     * 
     * Provide list of food data according to searched parameters
     * 
     * @param $data contains parameters on which search has to be taken place
     * 
     * @throws Exception If something happens during the process
     * 
     * @return user(expert) objects with success if every thing went right otherwise failure 
     * 
     */
    public function search($data) {
        try {
            $response = $this->apiObj->request('GET', $this->apiConfig['endpoint'] . 'search', [
                'query' => ['api_key' => $this->apiConfig['apikey'], 'format' => 'json',
                    'q' => $data['q'], 'sort' => 'n', 'max' => 10, 'offset' => 0]
            ]);
            $content = json_decode($response->getBody());
            return  new FoodCollection(collect($content->list->item));
            //dd($responseData->resource);
            //return $this->renderSuccess(trans('messages.foodproduct_retrieved_successfully'), $responseData);
        } catch (\Exception $e) {
            $this->logError(__METHOD__, $e);
            return $this->renderFailure(trans('messages.error.exception'), Response::HTTP_OK);
        }
    }

    public function getEnergyNutrientFromReportsApi(Cart $cart) {
        try {
            $response = $this->apiObj->request('GET', $this->apiConfig['endpoint'] . 'V2/reports', [
                'query' => ['api_key' => $this->apiConfig['apikey'], 'format' => 'json',
                    'ndbno' => $cart->item_id, 'type' => 'b']
            ]);
            $content = json_decode($response->getBody(), true);

            if ($content['foods'][0]['food']['nutrients']) {
                foreach ($content['foods'][0]['food']['nutrients'] as $nutrient) {
                    if (strtolower($nutrient['name']) == 'energy') {
                        break;
                    }
                }
            }
            return $nutrient['value']??null;
        } catch (\Exception $e) {
            $this->logError(__METHOD__, $e);
            return null;
        }
    }

}
