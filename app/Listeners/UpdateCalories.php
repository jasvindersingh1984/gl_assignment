<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ItemAdded;
use Log;
use App\Implementers\FoodContractImpl;

class UpdateCalories implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ItemAdded $event)
    {
        try {
            Log::info('Start');
            $foodcontractImpl = new FoodContractImpl;
            $calories = $foodcontractImpl->getEnergyNutrientFromReportsApi($event->cartItem);
            Log::info('For cartItem '.$event->cartItem.' Calories are :: '.$calories);
            $event->cartItem->updateCartItem($calories);
            Log::info('Done');
        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
