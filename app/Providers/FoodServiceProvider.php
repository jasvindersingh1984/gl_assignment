<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Models\Cart;
use App\Observers\CartObserver;

class FoodServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\FoodContract', 'App\Implementers\FoodContractImpl');
        //Cart::observe(CartObserver::class);
    }
}
