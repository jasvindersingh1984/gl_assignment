<?php

namespace App\Observers;

use App\Http\Models\Cart;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use Log;

class CartObserver
{
    /**
     * Handle the cart "created" event.
     *
     * @param  \App\Http\Models\Cart  $cart
     * @return void
     */
    public function created(Cart $cart)
    {
        try {
            Log::info('dasdasa');
            $usda = Config::get('services.usda');
            $client = new Client();
            $response = $client->request('GET', $usda['endpoint'].'V2/reports', [
                'query' => ['api_key' => $usda['apikey'],'format'=>'json',
                    'ndbno'=>$cart->item_id,'type'=>'b']
            ]);
            $content = json_decode($response->getBody(), true);
            if($content['foods'][0]['food']['nutrients']){
                foreach ($content['foods'][0]['food']['nutrients'] as $nutrient){
                    if(strtolower($nutrient['name'])=='energy'){
                        Log::info($nutrient);
                        $cart->calories=$nutrient['value'];
                        break;
                    }
                }
                $cart->save();
            }
            Log::info('done');
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Handle the cart "updated" event.
     *
     * @param  \App\Cart  $cart
     * @return void
     */
    public function updated(Cart $cart)
    {
        //
    }

    /**
     * Handle the cart "deleted" event.
     *
     * @param  \App\Cart  $cart
     * @return void
     */
    public function deleted(Cart $cart)
    {
        //
    }

    /**
     * Handle the cart "restored" event.
     *
     * @param  \App\Cart  $cart
     * @return void
     */
    public function restored(Cart $cart)
    {
        //
    }

    /**
     * Handle the cart "force deleted" event.
     *
     * @param  \App\Cart  $cart
     * @return void
     */
    public function forceDeleted(Cart $cart)
    {
        //
    }
}
