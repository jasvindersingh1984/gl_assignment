<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\FoodContract;

class FoodController extends BaseController
{
    protected $food;
    public function __construct(FoodContract $food) {
        parent::__construct();
        $this->food =$food;
    }
    
    public function search(Request $request) {
        return $this->food->search($request);
    }
    
}
