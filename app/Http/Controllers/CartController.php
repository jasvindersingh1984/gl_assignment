<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\CartContract;

class CartController extends BaseController
{
    protected $cart;
    public function __construct(CartContract $cart) {
        parent::__construct();
        $this->cart =$cart;
    }
    
    public function addtolist(Request $request) {
        return $this->cart->addtolist($request);
    }
    
    public function viewList() {
        return $this->cart->viewList();
    }
}
