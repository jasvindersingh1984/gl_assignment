<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = ['item_id','item_name'];
    protected $hidden = ['created_at','updated_at'];
    
    public function addToCart($data){
        $this->fill($data);
        $this->save();
    }
    public function updateCartItem($calories){
        if($calories!=null){
            $this->calories=$calories;
            $this->save();
        }
    }
}
