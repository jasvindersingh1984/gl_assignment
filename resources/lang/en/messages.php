<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'error' => ['exception'=>'Something went wrong.'],
    'item_added_successfully'=>'Cart item added successfully.',
    'list_retrieved_successfully' => 'Cart item list retrieved successfully.',

];
