## Project Setup
cd pathtoprojectdir
mkdir projectname
cd projectname
git clone https://jasvindersingh1984@bitbucket.org/jasvindersingh1984/gl_assignment.git .
chmod -R 777 storage bootstrap

## Update .env config(database & api) 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DBNAME
DB_USERNAME=root    
DB_PASSWORD=root

USDA_KEY=FwIrbOhuhcvfuvzkyDOzmpni7lvy02AXIg3s3h9u
USDA_ENDPOINT=https://api.nal.usda.gov/ndb/

## Execute database migration
composer update 
php artisan migrate
php artisan key:generate

## vhost Setup 
edit hosts file
vim /etc/hosts
add below line
127.0.0.1       localgl.com
add vhost directive
vim /etc/apache2/extra/httpd-vhosts.conf
<VirtualHost *:80>
    ServerName localgl.com
    ServerAlias www.localgl.com
    DocumentRoot "/Library/WebServer/Documents/gl_assignment/public"
    ErrorLog "/private/var/log/apache2/localgl.com-error_log"
    CustomLog "/private/var/log/apache2/localgl.com-access_log" common
</VirtualHost>

restart apache2
sudo service apache2 restart


## Queue execute -- updating cart item calories value via laravel queue
cd pathtoproject
php artisan queue:work
