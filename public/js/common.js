$(document).ready(function () {
    $("#search").click(function () {
        var q = $('#q').val();
        $.get("search?q=" + q, function (data, status) {
            if(data.success==1){
                $('#div1').html('');
                $.each(data.data, function(index, item){
                    let innerdivhtml = '<div class="basket-item"><input class="button-blue" type="button" data-item_id="'+item.itemId+'" data-item_name="'+item.itemName+'" value="addtolist"> '+item.itemName+'</div>';
                    $('#div1').append(innerdivhtml);
                })
            }
        });
    });
    
    $(document).on('click',".button-blue",function () {
        let productObj = $(this).data();
        console.log(productObj);
        $.post("add-to-list", productObj, function (data, status) {
            if(data.success==1){
                let basketCount = $('#basketCount').html();
                $('#basketCount').html(++basketCount);
            }
        });
    });
    
    $(document).on('click',"#basketCount",function () {
        $('#basketListTable').find("tr:gt(0)").remove();
        $.get("view-list", function (data, status) {
            if(data.success==1){
                $.each(data.result.items, function(index, item){
                    let basketItem = '<tr><td>'+item.itemName+'</td><td>'+item.calories+'</td></tr>';
                    $('#basketListTable').append(basketItem);
                });
                let total = '<tr><td><b>Total Calories</b></td><td><b>'+data.result.total+'</b></td></tr>';
                $('#basketListTable').append(total);
                $('#basketListTable').show();
            }
        });
    });
});